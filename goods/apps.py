"""
Module app
"""
from django.apps import AppConfig


class GoodsConfig(AppConfig):
    """
    Class with application name
    """
    name = 'goods'
