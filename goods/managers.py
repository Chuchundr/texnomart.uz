from libraries.base import BaseManager


class CategoriesManager(BaseManager):
    """
    manager for categories
    """
    URI = '/api/goods/categories/'
