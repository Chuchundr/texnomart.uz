from django.db import models
from address import models as address_models


class Store(models.Model):
    name = models.CharField(max_length=100, verbose_name="Название Склада")
    city = models.ForeignKey(
        address_models.City,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name="Город"
    )
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return self.name

    def to_dict(self):
        """
        returns dict
        """
        return {
            'name': self.name,
            'city': self.city,
        }

    class Meta:
        verbose_name = 'Название Склада'
        verbose_name_plural = 'Название Склада'
