"""
package: clients
description: apps
"""
from django.apps import AppConfig


class ClientsConfig(AppConfig):
    """
    конфигурация для приложения clients
    """
    name = 'clients'
    verbose_name = 'Клиенты'

    def ready(self):
        import clients.signals
