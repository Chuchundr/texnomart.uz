"""
package: clients
description: views
"""
from django.views import generic
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import AuthenticationForm
from modules import pass_gen
from . import models, forms
from goods import models as goods_models


class PhoneVerificationView(generic.CreateView):
    """
    представление для регистрации пользователя
    с указанием телефонного номера
    """
    form_class = forms.PhoneNumberForm
    template_name = 'clients/phone_verification.html'

    def get_success_url(self):
        return reverse('clients:registration',
                       kwargs={'slug': self.object.slug})


class RegisterView(generic.CreateView):
    """
    представление для продолжения регистрации, для
    указания данных пользователя
    """
    form_class = forms.UserForm
    template_name = 'clients/registration.html'

    def get_context_data(self, *args, **kwargs):  # pylint: disable=arguments-differ
        context = super(RegisterView, self).get_context_data(*args, **kwargs)
        context['personal_form'] = forms.PersonalDataForm
        return context

    def form_valid(self, form):
        user = form.save(commit=False)
        new_password = pass_gen.gen_pw()
        user.set_password(new_password)
        user.save()
        personal = forms.PersonalDataForm(self.request.POST).save()
        phone = models.Contacts.objects.get(slug=self.kwargs.get('slug'))
        phone.user = user
        phone.save()
        personal.user = user
        personal.save()
        new_user = authenticate(username=user.username,
                                password=new_password)
        login(self.request, new_user)
        return redirect(self.get_success_url())

    def get_success_url(self):
        return reverse('tm_site:index')


class LoginView(generic.FormView):
    """
    представдение для авторизации
    """
    form_class = AuthenticationForm
    template_name = 'clients/login.html'

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return super(LoginView, self).form_valid(form)

    def get_success_url(self):
        return reverse('tm_site:index')


class LogoutView(generic.base.View):
    """
    представление для выхода из учетной записи
    """
    def get(self, request):
        """
        метод для логаута
        """
        logout(request)
        return HttpResponseRedirect(reverse('tm_site:index'))
