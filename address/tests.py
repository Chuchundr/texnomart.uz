"""
Module test
"""
from django.test import TestCase
from django.test import Client
from address import models


class AddressTestCase(TestCase):
    """
    тесты для приложения address
    """

    fixtures = ['fixture_address.json']

    def setUp(self):
        self.client = Client()
        self.country = models.Country.objects.last()
        self.region = models.Region.objects.last()
        self.city = models.City.objects.last()
        self.area = models.Area.objects.last()
        self.street = models.Street.objects.last()
        self.address = models.Address.objects.last()


    def test_country_str(self):
        """
        проверка метода str для модели country
        """
        self.assertEqual(self.country.__str__(), 'Узбекистан')

    def test_country_save(self):
        """
        проверка сохранения ЧПУ для модели country
        """
        self.country.save()
        self.assertEqual(self.country.slug, 'uzbekistan')

    def test_region_str(self):
        """
        проверка метода str для модели region
        """
        self.assertEqual(self.region.__str__(), 'Ташкентская обл.')

    def test_region_save(self):
        """
        проверка созранения ЧПУ для модели region
        """
        self.region.save()
        self.assertEqual(self.region.slug, 'tashkentskaya-obl')

    def test_city_str(self):
        """
        проверка метода str для модели city
        """
        self.assertEqual(self.city.__str__(), 'Ташкент')

    def test_city_save(self):
        """
        проверка сохранения ЧПУ для модели city
        """
        self.city.save()
        self.assertEqual(self.city.slug, 'tashkent')

    def test_area_str(self):
        """
        проверка метода str для модели area
        """
        self.assertEqual(self.area.__str__(), 'Мирабадский район')

    def test_area_save(self):
        """
        проверка сохранения ЧПУ для модели area
        """
        self.area.save()
        self.assertEqual(self.area.slug, 'mirabadskij-rajon')

    def test_street_str(self):
        """
        проверка метода str для модели street
        """
        self.assertEqual(self.street.__str__(), 'ш руставели')

    def test_street_save(self):
        """
        проверка сохранения ЧПУ для модели street
        """
        self.street.save()
        self.assertEqual(self.street.slug, 'sh-rustaveli')

    def test_address_str(self):
        """
        проверка метода str для модели address
        """
        self.assertEqual(self.address.__str__(), 'Узбекистан Ташкент Мирабадский район ш руставели 1')

    def test_address_save(self):
        """
        проверка сохранения ЧПУ для модели address
        """
        self.address.save()
        self.assertEqual(self.address.slug, 'uzbekistan-tashkent-mirabadskij-rajon-sh-rustaveli-1')
