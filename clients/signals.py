"""
package: clients
description: signals
"""
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from clients import tasks


@receiver(post_save, sender=User)
def create_pass_hook(sender, instance, using, **kwargs): # pylint: disable=unused-argument
    """
    сигнал запускающий отправку пароля на почту клиента
    """
    tasks.send_password.delay()
