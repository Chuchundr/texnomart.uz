"""
Module models
"""
import decimal
from django.db import models
from marketing import models as market_models


class KreditTime(models.Model):
    """
    Model KreditTime
    """
    value = models.IntegerField(unique=True, verbose_name="Временной интервал")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return str(self.value)

    def to_dict(self):
        """
        returns dict
        """
        return {
            'value': self.value
        }

    class Meta:
        verbose_name = 'Временной интервал'
        verbose_name_plural = 'Временной интервал'


class Pricing(models.Model):
    """
    Model Pricing
    """
    price = models.DecimalField(max_digits=50, decimal_places=2, verbose_name="Цена/Сум")
    bonus_card = models.ForeignKey(market_models.BonusCard, on_delete=models.CASCADE, verbose_name="Бонусная карта")
    kredit_time = models.ForeignKey(KreditTime, on_delete=models.CASCADE, verbose_name="Кол-во месяцев")
    prepayment = models.DecimalField(max_digits=50, decimal_places=2, null=True, blank=True, verbose_name="Предоплата/Сум")
    monthly = models.DecimalField(max_digits=50, decimal_places=2, null=True, blank=True, verbose_name="Ежемесячно/Сум")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return str(self.price)

    def to_dict(self):
        """
        Function returns dict
        """
        return {
            'price': str(self.price),
            'bonus_card': self.bonus_card,
            'kredit_time': self.kredit_time,
            'prepayment': str(self.prepayment),
            'monthly': str(self.monthly),
        }

    def save(self, *args, **kwargs): # pylint: disable=arguments-differ
        self.prepayment = self.price / 100 * self.bonus_card.percent
        price = self.price - self.prepayment
        self.monthly = price / self.kredit_time.value
        super(Pricing, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Ценообразование'
        verbose_name_plural = 'Ценообразование'
