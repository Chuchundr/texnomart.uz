from django.test import TestCase
from marketing import models


class MarketingTestCase(TestCase):

    fixtures = ['fixture_marketing.json']

    def setUp(self) -> None:
        self.bonus_card = models.BonusCard.objects.last()

    def test_bonus_card_str(self):
        self.assertEqual(self.bonus_card.__str__(), 'Member')

