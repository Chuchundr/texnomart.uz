"""
Module test
"""
from decimal import Decimal
from django.test import TestCase
from goods import models

class GoodsTestCase(TestCase):

    fixtures = ['fixture_goods.json']

    def setUp(self) -> None:
        self.categories = models.Categories.objects.last()
        self.category = models.Category.objects.last()
        self.good_name = models.GoodName.objects.last()
        self.good = models.Good.objects.last()
        self.specification_name = models.SpecificationName.objects.last()
        self.specification_name_bool = models.SpecificationNameBool.objects.last()
        self.specification_value = models.SpecificationValue.objects.last()
        self.specifictaion = models.Specification.objects.last()
        self.specification_bool = models.SpecificationBool.objects.last()

    def test_categories_str(self):
        self.assertEqual(self.categories.__str__(), 'parent category')

    def test_category_str(self):
        self.assertEqual(self.category.__str__(), 'category')

    def test_good_name_str(self):
        self.assertEqual(self.good_name.__str__(), 'good')

    def test_good_str(self):
        self.assertEqual(self.good.__str__(), "('good', '1')")

    def test_good_save(self):
        self.good.save()
        self.assertEqual(self.good.slug, 'category-1')
        self.assertTrue(self.good.pricing)
        self.assertEqual(self.good.monthly, Decimal('0.00'))
        self.assertTrue(self.good.is_prepayment)
        self.assertEqual(self.good.prepayment, 0)

    def test_specification_name_str(self):
        self.assertEqual(self.specification_name.__str__(), 'глаза')

    def test_specification_name_bool_str(self):
        self.assertEqual(self.specification_name_bool.__str__(), 'живое')

    def test_specification_value_str(self):
        self.assertEqual(self.specification_value.__str__(), 'круглые')

    def test_specification(self):
        self.assertEqual(self.specifictaion.__str__(), '(<SpecificationName: глаза>, <SpecificationValue: круглые>)')

    def test_specification_bool(self):
        self.assertEqual(self.specification_bool.__str__(), "('живое', True)")
