"""
package: tm_site
description: views
"""
from django.views import generic
from goods import models as goods_models


class MainPageView(generic.TemplateView):
    template_name = 'index.html'

    def get_context_data(self):
        context = super(MainPageView, self).get_context_data()
        context['goods'] = goods_models.Good.objects.all()
        return context


class GoodDetailView(generic.DetailView):
    model = goods_models.Good
    template_name = 'good_detail.html'
