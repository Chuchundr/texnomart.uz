"""
Module APP
"""
from django.apps import AppConfig


class AddressConfig(AppConfig):
    """
    Class with application name
    """
    name = 'address'
