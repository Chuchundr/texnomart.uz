from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import reverse
from clients import models
from clients import forms


class ClientsTestCase(TestCase):

    fixtures = ['fixture_clients.json']
    
    def setUp(self):
        self.client = Client()
        self.contacts = models.Contacts.objects.last()
        self.personal = models.PersonalData.objects.last()

    def test_contacts_str(self):
        self.assertEqual(self.contacts.__str__(), '+998977441281 farruh1607@mail.ru')

    def test_contacts_save(self):
        self.contacts.save()
        self.assertEqual(self.contacts.slug, '998977441281')

    def test_personal_data_str(self):
        self.assertEqual(self.personal.__str__(), 'Фаррух Фазылджанов Тулкунович')

    def test_phone_verification_success_url(self):
        response = self.client.post(reverse('clients:registration-contacts'), {'phone_number1': '+998712905110', 'email': 'dreamquestff@gmail.com'})
        self.assertRedirects(response, reverse('clients:registration', kwargs={'slug': '998712905110'}))

    def test_register_view_context(self):
        response = self.client.get(reverse('clients:registration', kwargs={'slug': self.contacts.slug}))
        self.assertIs(response.context['personal_form'], forms.PersonalDataForm)

    def test_register_view_form_valid(self):
        self.client.post(reverse('clients:registration', kwargs={'slug': self.contacts.slug}),
                                    {
                                        'username': 'some_username',
                                        'first_name': 'some_name',
                                        'last_name': 'some_lastname',
                                        'middle_name': 'some_middle_nam'
                                    })
        self.assertEqual(User.objects.last().username, 'some_username')

    def test_login_view_form_valid(self):
        response = self.client.post(reverse('clients:login'), {'username': 'staff', 'password': 'QazEdcwsxrfv'})
        self.assertRedirects(response, reverse('tm_site:index'))

    def test_logout(self):
        response = self.client.get(reverse('clients:logout'))
        self.assertRedirects(response, reverse('tm_site:index'))
