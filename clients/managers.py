from libraries.base import BaseManager


class ContactsManager(BaseManager):
    """
    manager for clients
    """
    URI = '/api/clients/contacts/'
