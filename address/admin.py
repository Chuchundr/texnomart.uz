"""
Module admin
"""
from django.contrib import admin
from address import models


@admin.register(models.Country)
class CountryAdmin(admin.ModelAdmin):
    """
    Class admin for model Country
    """
    list_display = ['country']
    search_fields = ['country']
    list_filter = ['country']


@admin.register(models.Region)
class RegionAdmin(admin.ModelAdmin):
    """
    Class admin for model Region
    """
    list_display = ['region']
    search_fields = ['region']
    list_filter = ['region']
    autocomplete_fields = ['parent']


@admin.register(models.City)
class CityAdmin(admin.ModelAdmin):
    """
    Class admin for model City
    """
    list_display = ['city']
    search_fields = ['city']
    list_filter = ['city']
    autocomplete_fields = ['parent']


@admin.register(models.Area)
class AreaAdmin(admin.ModelAdmin):
    """
    Class admin for model Area
    """
    list_display = ['area']
    search_fields = ['area']
    list_filter = ['area']
    autocomplete_fields = ['parent']


@admin.register(models.Street)
class StreetAdmin(admin.ModelAdmin):
    """
    Class admin for model Street
    """
    list_display = ['street']
    search_fields = ['street']
    list_filter = ['street']
    autocomplete_fields = ['parent']


@admin.register(models.Address)
class AddressAdmin(admin.ModelAdmin):
    """
    Class admin for model Address
    """
    list_display = ['country', 'region', 'city', 'area', 'street', 'house', 'flat']
    search_fields = ['country__country',
                     'region__region',
                     'city__city',
                     'area__area',
                     'street__street',
                     'house',
                     'flat']
    list_filter = ['country__country', 'region', 'city', 'area', 'street__street', 'house', 'flat']
