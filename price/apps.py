"""
Module apps
"""
from django.apps import AppConfig


class PriceConfig(AppConfig):
    """
    Class with application name
    """
    name = 'price'
