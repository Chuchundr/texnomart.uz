"""
package: clients
description: admin
"""
from django.contrib import admin
from clients import models


@admin.register(models.Contacts)
class PhoneNumberAdmin(admin.ModelAdmin):
    """
    телефонные номера клиентов в админ панели
    """
    list_display = ['phone_number1', 'phone_number2', 'email', 'slug']
    list_filter = ['phone_number1', 'phone_number2', 'email']


@admin.register(models.PersonalData)
class PersonalDataAdmin(admin.ModelAdmin):
    """
    персональные данные
    """
    list_display = ['first_name', 'last_name', 'middle_name']
