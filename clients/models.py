"""
package: clients
description: models
"""
from django.db import models
from django.contrib.auth.models import User
from pytils.translit import slugify  # для перевода кирилицы


class Contacts(models.Model):
    """
    модель для контактных данных клиента
    """
    phone_number1 = models.CharField(max_length=50, unique=True, verbose_name='Номер телефона')
    phone_number2 = models.CharField(max_length=50, blank=True, verbose_name='Запасной номер')
    email = models.CharField(max_length=100, blank=True, verbose_name='Адрес электронной почты')
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, verbose_name='Клиент')
    slug = models.SlugField(max_length=200, editable=False, verbose_name='ЧПУ')

    def __str__(self):
        return f'{self.phone_number1} {self.email}'

    def save(self):  # pylint: disable=arguments-differ
        self.slug = slugify(f'{self.phone_number1}')
        super(Contacts, self).save()

    def to_dict(self):
        """
        returns dict
        """
        return {
            'phone_number1': self.phone_number1,
            'phone_number2': self.phone_number2,
            'email': self.email,
            'user': self.user.id
        }

    class Meta:
        verbose_name = 'контакт'
        verbose_name_plural = 'контакты'


class PersonalData(models.Model):
    """
    Модель для личных данных клиента
    """
    first_name = models.CharField(max_length=100, verbose_name='Имя')
    last_name = models.CharField(max_length=100, verbose_name='Фамилия')
    middle_name = models.CharField(max_length=100, verbose_name='Отчество')
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name} {self.middle_name}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'first_name': self.first_name,
            'last_name': self.last_name,
            'middle_name': self.middle_name,
            'user': self.user.id
        }

    class Meta:
        verbose_name = 'Личные данные'
        verbose_name_plural = 'личные данные'
