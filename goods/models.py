"""
Module models
"""
from django.db import models
from django.urls import reverse
from pytils.translit import slugify
from price import models as price_models
from stores import models as store_models


class Categories(models.Model):
    """
    Model Categories
    """
    name = models.CharField(max_length=100, unique=True, verbose_name="Название Родительской категории")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return f'{self.name}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'name': self.name,
        }
    
    class Meta:
        verbose_name = 'Родительскую Категорию'
        verbose_name_plural = 'Родительская Категория'


class Category(models.Model):
    """
    Model Category
    """
    parent = models.ForeignKey(
        Categories,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        verbose_name="Родительская категория"
    )
    name = models.CharField(max_length=100, unique=True, verbose_name="Название категории")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return f'{self.name}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'parent': self.parent.id,
            'name': self.name
        }
    
    class Meta:
        verbose_name = 'Категорию'
        verbose_name_plural = 'Категория'


class GoodName(models.Model):
    """
    Model GoodName
    """
    name = models.CharField(max_length=100, verbose_name="Название Товара")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return f'{self.name}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'name': self.name
        }

    class Meta:
        verbose_name = 'Название Товара'
        verbose_name_plural = 'Название Товара'


class Good(models.Model):
    """
    Model Good
    """
    store = models.ForeignKey(store_models.Store, blank=True, null=True, on_delete=models.CASCADE, verbose_name="Склад")
    name = models.ForeignKey(GoodName, on_delete=models.CASCADE, verbose_name="Название Товара")
    photo = models.ImageField(blank=True, upload_to='good-photo', verbose_name='Фото товара')
    model = models.CharField(max_length=200, verbose_name="Модель")
    pricing = models.ForeignKey(
        price_models.Pricing,
        null=True, blank=True,
        on_delete=models.CASCADE,
        verbose_name="Цена в рассрочку/Сум"
    )
    prepayment = models.IntegerField(blank=True, null=True, verbose_name="Предоплата/Сум")
    is_prepayment = models.BooleanField(default=True, verbose_name="Есть преоплата")
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name="Категория Товара")
    in_stock = models.BooleanField(default=False, verbose_name="В наличии")
    monthly = models.IntegerField(blank=True, null=True, verbose_name="Оплата в месяц/Сум")
    slug = models.SlugField(max_length=250, editable=False, db_index=True, blank=True, verbose_name='ЧПУ')
    description = models.TextField(max_length=500, null=True, blank=True, verbose_name="Описание товара")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def get_absolute_url(self):
        """
        Function returns url
        """
        return reverse('goods:good-detail', kwargs={'slug': self.slug})

    def __str__(self):
        return f'{self.name.name, self.model}'

    def save(self, *args, **kwargs):  # pylint: disable=arguments-differ
        self.slug = slugify(f'{self.category}-{self.model}')
        if self.pricing is not None:
            self.monthly = self.pricing.monthly
        if self.is_prepayment is False:
            self.prepayment = 0
        else:
            self.prepayment = self.pricing.prepayment
        super(Good, self).save(*args, **kwargs)

    def to_dict(self):
        """
        returns dict
        """
        return {
            'store': self.store.id,
            'name': self.name.id,
            'model': self.model,
            'pricing': self.pricing,
            'prepayment': self.prepayment,
            'is_prepayment': self.is_prepayment,
            'category': self.category.id,
            'in_stock': self.in_stock,
            'monthly': self.monthly,
            'description': self.description
        }

    class Meta:
        unique_together = ['name', 'model', 'category', 'pricing']
        verbose_name = 'Товар'
        verbose_name_plural = 'Товар'


class SpecificationName(models.Model):
    """
    Model SpecificationName
    """
    name = models.CharField(max_length=250, unique=True, verbose_name="Название характеристики")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return f'{self.name}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'name': self.name
        }

    class Meta:
        verbose_name = 'Название Спецификации для Текстовых'
        verbose_name_plural = 'Название Спецификации для Текстовых'


class SpecificationNameBool(models.Model):
    """
    Model SpecificationNameBool
    """
    name = models.CharField(max_length=250, unique=True, verbose_name="Название характеристики")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return f'{self.name}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'name': self.name
        }

    class Meta:
        verbose_name = 'Название Спецификации для Булево'
        verbose_name_plural = 'Название Спецификации для Булево'


class SpecificationValue(models.Model):
    """
    Model SpecificationValue
    """
    value = models.CharField(max_length=250, unique=True, verbose_name="Значение характеристики")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return f'{self.value}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'value': self.value
        }

    class Meta:
        verbose_name = 'Значение Спецификации'
        verbose_name_plural = 'Значение Спецификации'


class Specification(models.Model):
    """
    Model Specification
    """
    name = models.ForeignKey(SpecificationName, on_delete=models.CASCADE, verbose_name="Название характеристики")
    value = models.ForeignKey(SpecificationValue, on_delete=models.CASCADE, verbose_name="Значение характеристики")
    good = models.ForeignKey(Good, on_delete=models.CASCADE, verbose_name="Название товара")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return f'{self.name.name}, {self.value.value}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'name': self.name,
            'value': self.value,
            'good': self.good.id
        }

    class Meta:
        verbose_name = 'Спецификацию'
        verbose_name_plural = 'Спецификация'


class SpecificationBool(models.Model):
    """
    Model SpecificationBool
    """
    name = models.ForeignKey(SpecificationNameBool, on_delete=models.CASCADE, verbose_name="Название характеристики")
    value = models.BooleanField(default=False, verbose_name="Значение характеристики")
    good = models.ForeignKey(Good, on_delete=models.CASCADE, verbose_name="Название товара")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return f'{self.name.name}, {self.value}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'name': self.name,
            'value': self.value,
            'good': self.good.id
        }

    class Meta:
        verbose_name = 'Спецификацию для Булево'
        verbose_name_plural = 'Спецификация для Булево'
