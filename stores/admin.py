from django.contrib import admin
from stores import models


@admin.register(models.Store)
class StoreAdmin(admin.ModelAdmin):
    """
    Класс для Товара
    """
    list_display = ['name']
    search_fields = ['name']
    list_filter = ['name']