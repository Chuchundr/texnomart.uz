from django.contrib import admin
from . import models


@admin.register(models.BonusCard)
class BonusCardAdmin(admin.ModelAdmin):
    model = models.BonusCard
    list_display = ['name', 'percent', 'bonus', 'limit']
