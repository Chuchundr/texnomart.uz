"""
Module test
"""
from decimal import Decimal
from django.test import TestCase
from price import models


class PriceTestCase(TestCase):

    fixtures = ['fixture_price.json']

    def setUp(self) -> None:
        self.credit_time = models.KreditTime.objects.last()
        self.pricing = models.Pricing.objects.last()

    def test_credit_time_str(self):
        self.assertEqual(self.credit_time.__str__(), '1')

    def test_pricing_str(self):
        self.assertEqual(self.pricing.__str__(), '1.00')

    def test_pricing_save(self):
        self.pricing.save()
        self.assertEqual(self.pricing.prepayment, Decimal('0.01'))
        self.assertEqual(self.pricing.monthly, Decimal('0.99'))

    def test_pricing_to_dict(self):
        self.assertIsInstance(self.pricing.to_dict(), dict)

